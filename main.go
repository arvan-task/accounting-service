package main

import (
	"arvan_accounting_service/infrastructure/persistence"
	"arvan_accounting_service/interfaces"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func main(){
	config, err := LoadConfiguration(os.Args[1:])
	if err != nil {
		panic(err)
	}

	services, err := persistence.NewRepositories(config.Database.Driver,
		config.Database.Username,
		config.Database.Password,
		config.Database.Port,
		config.Database.Host,
		config.Database.Database)
	if err != nil {
		panic(err)
	}
	defer services.Close()
	services.AutoMigrate()

	accounts := interfaces.NewAccount(services.Account)

	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	v1 := router.Group("v1")
	{
		accountGroup := v1.Group("/accounts")
		{
			balanceGroup := accountGroup.Group(":phone-number/balance")
			{
				balanceGroup.GET("", accounts.GetBalance)
				balanceGroup.POST("/change", accounts.ChangeAccountBalance)
			}
		}
	}
	log.Fatal(router.Run(config.HttpServer.Address))
}