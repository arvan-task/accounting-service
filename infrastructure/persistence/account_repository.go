package persistence

import (
	"arvan_accounting_service/domain/entity"
	"arvan_accounting_service/domain/repository"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

type AccountRepository struct {
	db *gorm.DB
}

var _ repository.AccountRepository = &AccountRepository{}

func (a AccountRepository) GetAccount(id int64) (*entity.Account, error) {
	account := entity.Account{}
	err := a.db.Model(&account).Find(&account, "id=?", id).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrap(repository.ErrAccountNotFound, "account not found")
		} else {
			return nil, errors.New("database problem")
		}
	}
	return &account, nil
}

func (a AccountRepository) GetAccountByPhoneNumber(phoneNumber string) (*entity.Account, error) {

	var account entity.Account
	err := a.db.Model(account).Find(&account, "phone_number=?", phoneNumber).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrap(repository.ErrAccountNotFound, "account not found")
		} else {
			return nil, errors.New("database problem")
		}
	}
	return &account, nil
}

func (a AccountRepository) UpdateAccount(account *entity.Account) (*entity.Account, error) {
	err := a.db.Model(&account).Where("id=?", account.ID).Save(&account).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrap(repository.ErrAccountNotFound, "account not found")
		} else {
			return nil, errors.New("database problem")
		}
	}
	return account, nil
}

func NewAccountRepository(db *gorm.DB) *AccountRepository {
	return &AccountRepository{db}
}
