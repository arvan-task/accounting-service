package persistence

import (
	"arvan_accounting_service/domain/entity"
	"arvan_accounting_service/domain/repository"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Repositories struct {
	Account repository.AccountRepository
	db           *gorm.DB
}

func NewRepositories(DbDriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*Repositories, error) {
	databaseConnectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?parseTime=true", DbUser, DbPassword, DbHost, DbPort, DbName)
	db, err := gorm.Open(DbDriver, databaseConnectionString)
	if err != nil {
		return nil, err
	}
	db.LogMode(true)

	return &Repositories{
		Account: NewAccountRepository(db),
		db:           db,
	}, nil
}

func (s *Repositories) Close() error {
	return s.db.Close()
}

func (s *Repositories) AutoMigrate() error {
	return s.db.AutoMigrate(&entity.Account{}).Error
}