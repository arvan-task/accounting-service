package interfaces

const (
	invalidPhoneNumber         = "INVALID_PHONE_NUMBER"
	accountNotFound            = "ACCOUNT_NOT_FOUND"
	invalidBalanceChangeAmount = "INVALID_BALANCE_CHANGE_AMOUNT"
	emptyBalanceChangeAmount   = "EMPTY_BALANCE_CHANGE_AMOUNT"
	notEnoughBalance           = "NOT_ENOUGH_BALANCE"
)
