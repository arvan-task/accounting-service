package interfaces

import (
	"arvan_accounting_service/application"
	"arvan_accounting_service/domain/repository"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"github.com/nyaruka/phonenumbers"
	"github.com/pkg/errors"
	"net/http"
	"strconv"
	"strings"
)

type Account struct {
	accountApp application.AccountAppInterface
}

func NewAccount(accountApp application.AccountAppInterface) *Account {
	return &Account{
		accountApp: accountApp,
	}
}

type getBalanceResponse struct {
	Balance float64 `json:"balance"`
}

func (a *Account) GetBalance(c *gin.Context) {

	phoneNumber := c.Param("phone-number")

	if !strings.HasPrefix(phoneNumber, "+") {
		phoneNumber = "+" + phoneNumber
	}

	phone, err := phonenumbers.Parse(phoneNumber, "")
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}
	isValidNumber := phonenumbers.IsValidNumber(phone)

	if !isValidNumber {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}

	account, err := a.accountApp.GetAccountByPhoneNumber(phoneNumber)
	if err != nil {
		if errors.Cause(err) == repository.ErrAccountNotFound {
			c.JSON(http.StatusNotFound, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
				ErrorType: accountNotFound,
				ErrorCode: http.StatusNotFound,
				Message:   accountNotFound}}}})
			return
		} else {
			c.JSON(http.StatusInternalServerError, nil)
			return
		}
	}

	c.JSON(http.StatusOK, BaseResponse{Data: getBalanceResponse{Balance: account.Balance},
		Meta: MetaResponse{Message: accountBalanceReturned,
			StatusCode: http.StatusOK}})
}

type changeAccountBalanceRequest struct {
	ChangeAmount float64 `json:"change_amount" form:"change_amount" binding:"required,number"`
}
type changeAccountBalanceResponse struct {
	NewBalance float64 `json:"new_balance"`
}

func changeAccountBalanceValidation(c *gin.Context, err error) Meta {
	var metaErrors []MetaError
	switch err.(type) {
	case validator.ValidationErrors:
		errors := err.(validator.ValidationErrors)
		for _, validationError := range errors {
			switch validationError.StructField() {
			case "ChangeAmount":
				if validationError.Tag() == "required" {
					metaErrors = append(metaErrors, MetaError{ErrorField: "change_amount",
						ErrorType: emptyBalanceChangeAmount,
						ErrorCode: http.StatusUnprocessableEntity,
						Message:   emptyBalanceChangeAmount})
				} else {
					metaErrors = append(metaErrors, MetaError{ErrorField: "change_amount",
						ErrorType: invalidBalanceChangeAmount,
						ErrorCode: http.StatusUnprocessableEntity,
						Message:   invalidBalanceChangeAmount})
				}

			}

		}

	case *strconv.NumError:
		_, err = strconv.ParseFloat(c.PostForm("change_amount"), 64)
		if err != nil {
			metaErrors = append(metaErrors, MetaError{ErrorField: "change_amount",
				ErrorType: invalidBalanceChangeAmount,
				ErrorCode: http.StatusUnprocessableEntity,
				Message:   invalidBalanceChangeAmount})

		}

	}

	return MetaErrors{Errors: metaErrors}
}
func (a *Account) ChangeAccountBalance(c *gin.Context) {
	var requestBody changeAccountBalanceRequest

	err := c.ShouldBind(&requestBody)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: changeAccountBalanceValidation(c, err)})
		return
	}
	phoneNumber := c.Param("phone-number")

	if !strings.HasPrefix(phoneNumber, "+") {
		phoneNumber = "+" + phoneNumber
	}

	phone, err := phonenumbers.Parse(phoneNumber, "")
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}
	isValidNumber := phonenumbers.IsValidNumber(phone)

	if !isValidNumber {
		c.JSON(http.StatusUnprocessableEntity, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorField: "phone_number",
			ErrorType:  invalidPhoneNumber,
			ErrorCode:  http.StatusUnprocessableEntity,
			Message:    invalidPhoneNumber}}}})
		return
	}

	account, err := a.accountApp.GetAccountByPhoneNumber(phoneNumber)
	if err != nil {
		if errors.Cause(err) == repository.ErrAccountNotFound {
			c.JSON(http.StatusNotFound, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
				ErrorType: accountNotFound,
				ErrorCode: http.StatusNotFound,
				Message:   accountNotFound}}}})
			return
		} else {
			c.JSON(http.StatusInternalServerError, nil)
			return
		}
	}

	account.Balance = account.Balance + requestBody.ChangeAmount

	if account.Balance < 0 {
		c.JSON(http.StatusBadRequest, BaseResponse{Meta: MetaErrors{Errors: []MetaError{{
			ErrorType: notEnoughBalance,
			ErrorCode: http.StatusBadRequest,
			Message:   notEnoughBalance}}}})
		return
	}

	account, err = a.accountApp.UpdateAccount(account)
	if err != nil {
		c.JSON(http.StatusInternalServerError, nil)
		return
	}

	c.JSON(http.StatusOK, BaseResponse{Data: changeAccountBalanceResponse{
		NewBalance: account.Balance,
	}, Meta: MetaResponse{StatusCode: http.StatusOK, Message: accountBalanceChanged}})
}
