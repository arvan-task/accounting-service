package main

import (
	"encoding/json"
	"flag"
	"os"
)

type Config struct {
	Database struct {
		Driver   string `json:"driver"`
		Host     string `json:"host"`
		Port     string `json:"port"`
		Username string `json:"username"`
		Password string `json:"password"`
		Database string `json:"database"`
	} `json:"database"`
	HttpServer struct {
		Address string `json:"address"`
	} `json:"http_server"`
}

func LoadConfiguration(args []string) (*Config, error) {

	var configFilePath string
	fs := flag.NewFlagSet("Arvan Accounting service", flag.ExitOnError)
	fs.StringVar(&configFilePath, "config", "", "config file for arvan accounting server")
	fs.StringVar(&configFilePath, "c", "", "config file for arvan accounting server")

	if err := fs.Parse(args); err != nil {
		return nil, err
	}

	configFile, err := os.Open(configFilePath)
	defer configFile.Close()
	if err != nil {
		return nil, err
	}
	jsonParser := json.NewDecoder(configFile)
	var outputConfig Config
	err = jsonParser.Decode(&outputConfig)
	if err != nil {
		return nil, err
	}
	return &outputConfig, nil
}
