package repository

import (
	"arvan_accounting_service/domain/entity"
	"errors"
)

type AccountRepository interface {
	GetAccount(id int64) (*entity.Account, error)
	GetAccountByPhoneNumber(phoneNumber string) (*entity.Account, error)
	UpdateAccount(account *entity.Account) (*entity.Account, error)
}

var (
	ErrAccountNotFound = errors.New("account not found")
)
