package entity

type Account struct {
	ID          int64   `json:"id"`
	PhoneNumber string  `json:"phone_number" gorm:"unique;size:15;not null"`
	Balance     float64 `json:"balance" gorm:"not null;default:0"`
}
