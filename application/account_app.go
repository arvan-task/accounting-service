package application

import (
	"arvan_accounting_service/domain/entity"
	"arvan_accounting_service/domain/repository"
)

type accountApp struct {
	accountRepository repository.AccountRepository
}

var _ AccountAppInterface = &accountApp{}

type AccountAppInterface interface {
	GetAccount(id int64) (*entity.Account, error)
	GetAccountByPhoneNumber(phoneNumber string) (*entity.Account, error)
	UpdateAccount(account *entity.Account) (*entity.Account, error)
}



func (a accountApp) GetAccount(id int64) (*entity.Account, error) {
	return a.accountRepository.GetAccount(id)
}

func (a accountApp) GetAccountByPhoneNumber(phoneNumber string) (*entity.Account, error) {
	return a.accountRepository.GetAccountByPhoneNumber(phoneNumber)
}

func (a accountApp) UpdateAccount(account *entity.Account) (*entity.Account, error) {
	return a.accountRepository.UpdateAccount(account)
}
