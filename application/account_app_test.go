package application

import (
	"arvan_accounting_service/domain/entity"
	"github.com/stretchr/testify/assert"
	"testing"
)

type fakeAccountRepo struct{}

func (f fakeAccountRepo) GetAccount(id int64) (*entity.Account, error) {
	return f.GetAccount(id)
}

func (f fakeAccountRepo) GetAccountByPhoneNumber(phoneNumber string) (*entity.Account, error) {
	return f.GetAccountByPhoneNumber(phoneNumber)
}

func (f fakeAccountRepo) UpdateAccount(account *entity.Account) (*entity.Account, error) {
	return f.UpdateAccount(account)
}

var (
	getAccountRepo              func(int64) (*entity.Account, error)
	getAccountByPhoneNumberRepo func(string) (*entity.Account, error)
	updateAccountRepo           func(*entity.Account) (*entity.Account, error)
)

var accountAppFake AccountAppInterface = &fakeAccountRepo{} //this is where the real implementation is swap with our fake implementation

func TestGetAccount_Success(t *testing.T) {
	//Mock the response coming from the infrastructure
	getAccountRepo = func(id int64) (*entity.Account, error) {
		return &entity.Account{
			ID:          1,
			PhoneNumber: "+989127788999",
			Balance:     9374593485,
		}, nil
	}
	accountId := int64(1)
	f, err := accountAppFake.GetAccount(accountId)
	assert.Nil(t, err)
	assert.EqualValues(t, f.ID, 1)
	assert.EqualValues(t, f.PhoneNumber, "+989127788999")
	assert.EqualValues(t, f.Balance, 9374593485)
}

func TestUpdateAccount_Success(t *testing.T) {
	//Mock the response coming from the infrastructure
	updateAccountRepo = func(user *entity.Account) (*entity.Account, error) {
		return &entity.Account{
			ID:          1,
			PhoneNumber: "+989127788999",
			Balance:     934592834,
		}, nil
	}
	account := &entity.Account{
		ID:          1,
		PhoneNumber: "+989127788999",
		Balance:     934592834,
	}
	a, err := accountAppFake.UpdateAccount(account)
	assert.Nil(t, err)
	assert.EqualValues(t, a.ID, 1)
	assert.EqualValues(t, a.PhoneNumber, "+989127788999")
	assert.EqualValues(t, a.Balance, 934592834)
}
