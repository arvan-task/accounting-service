module arvan_accounting_service

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.2.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/nyaruka/phonenumbers v1.0.59
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.6.1
)
